<div id="menubar" class=" ">
	<div class="menubar-scroll-panel">
		<ul id="main-menu" class="gui-controls">
			<li>
				<a href="{{ url('/dashboard') }}" class="active">
					<div class="gui-icon"><i class="md md-home"></i></div>
					<span class="title">Home</span>
				</a>
			</li>
			<li class="gui-folder">
				<a>
					<div class="gui-icon"><span class="md md-settings"></span></div>
					<span class="title">Mantenimientos</span>
				</a>
				<ul>
					<li><a href="{{ url('/categoria') }}" ><span class="title">Categoría</span></a></li>
					<li><a href="{{ url('/comercio') }}" ><span class="title">Comercios</span></a></li>
					<li><a href="{{ url('/cupones') }}" ><span class="title">Cupones</span></a></li>
				</ul>
			</li>
			<li class="gui-folder">
				<a>
					<div class="gui-icon"><span class="md md-assessment"></span></div>
					<span class="title">Reportes</span>
				</a>
				<ul>
					<li><a href="{{ url('/categorias') }}" ><span class="title">Categorías</span></a></li>
					<li><a href="{{ url('/cupones_redimidos') }}" ><span class="title">Cupones redimidos</span></a></li>
					<li><a href="{{ url('/usuarios') }}" ><span class="title">Usuarios</span></a></li>
				</ul>
			</li>
		</ul>
		<div class="menubar-foot-panel">
			<small class="no-linebreak hidden-folded">
				<span class="opacity-75">Copyright &copy; </span> <strong>GEOCUPON</strong>
			</small>
		</div>
	</div>
</div>