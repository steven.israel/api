<!-- jQuery Plugins -->
<script src="{{ asset('js/page/js/jquery.min.js') }}"></script>
<script src="{{ asset('js/page/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/page/js/slick.min.js') }}"></script>
<script src="{{ asset('js/page/js/nouislider.min.js') }}"></script>
<script src="{{ asset('js/page/js/jquery.zoom.min.js') }}"></script>
<script src="{{ asset('js/page/js/main.js') }}"></script>