<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>{{ $categoria->titulo }}</title>

 		@include('pagina/links')

    </head>
	<body>
		@include('pagina/menu')

		<!-- Section -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<div class="col-md-12">
						<div class="section-title text-center">
							<h3 class="title">Cupones {{ $categoria->titulo }}</h3>
						</div>
					</div>

					@foreach($cupones as $list)
					<!-- product -->
					<div class="col-md-3 col-xs-6">

						<div class="product">
							<div class="product-img">
								<img src="/images/Images_cupones/{{ $list->imagen }}" alt="">
								<div class="product-label">
									
								</div>
							</div>
							<div class="product-body">
								<p class="product-category">{{ $list->titulo }}</p>
								<h3 class="product-name"><a href="#"> {{ $list->descripcion }} </a></h3>
								<div class="product-rating">
								</div>
								<div class="product-btns">
									<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Canjeado {{ $list->redimido }}</span></button>
									<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Visto {{ $list->visto }} </span></button>
								</div>
							</div>
							<form class="form" method="POST" action="/canjear">
								{{ csrf_field() }}
								<div class="add-to-cart">
									<input type="hidden" name="cupon" value="{{ $list->idcupon }}">
									<input type="hidden" name="tipo" value="2">
									<input type="hidden" name="categoria" value="{{ $categoria->idcategoria }}">
									<button class="add-to-cart-btn"><i class="fa fa-plus-square"></i> Canjear</button>
								</div>
							</form>
						</div>
					</div>
					<!-- /product -->
					@endforeach
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /Section -->

		<div class="modal fade" id="myModalExito" tabindex="-1" role="dialog"> 
			<div class="modal-dialog"> <!-- Modal content--> 
				<div class="modal-content"> 
					<div class="modal-header"> 
						<form class="form" method="POST" action="/enviarCuponCategoria" enctype="multipart/form-data">
							{{ csrf_field() }}
							<!-- NEWSLETTER -->
							<div id="newsletter" class="section">
								@if(isset($cupon))
								<div class="">
									<!-- row -->
									<div class="row">
										<div class="product-img" align="center">
											<img src="/images/Images_cupones/{{ $cupon->imagen }}" width="150px">
											<div class="product-body">
												<p class="product-category">{{ $cupon->titulo }}</p>
												<h3 class="product-name"><a href="#">{{ $cupon->descripcion }}</a></h3>
											</div>
										</div>

										<div class="col-md-12">
											<div class="newsletter">
												<p>Enviar<strong>Cupon</strong> a mi correo</p>
												<form>
													<input type="hidden" name="idcupon" value="{{$cupon->idcupon}}">
													<input class="input" type="email" name="mail" placeholder="Ingrese su corre">
													<button class="newsletter-btn"><i class="fa fa-envelope"></i> Enviar</button>
												</form>
											</div>
										</div>
									</div>
									<!-- /row -->
								</div>
								@endif
							</div>
							<!-- /NEWSLETTER -->
						</form>         
					</div> 
				</div> 
			</div> 
		</div>


		@include('pagina/footer');

		<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript">
		   $(document).ready(function()
		   {	
		   		<?php if (isset($canjear)) { ?>
		      		$("#myModalExito").modal("show");
		      	<?php } ?>
		   });
		</script>

		@include('pagina/scripts');

	</body>
</html>
