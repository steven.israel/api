<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>GeoCupon</title>

	@include('pagina/links')

</head>
<body>

	@include('pagina/menu')

	<!-- SECTION -->
	<div class="section">
		<!-- container -->
		<div class="container" id="categorias">
			<!-- row -->
			<div class="row">
				@foreach($categorias as $list)
				<!-- shop -->
				<a href="{{ url('cupones', ['id' => $list->idcategoria]) }}">
					<div class="col-md-2 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="/images/images_categoria/{{ $list->logo }}" alt="">
							</div>
								<!-- <div class="shop-body">
									<h3>Electronicos<br></h3>
									<a href="#" class="cta-btn">Ver Cupon<i class="fa fa-arrow-circle-right"></i></a>
								</div> -->
							</div>
						</div>
					</a>
					@endforeach
					<!-- /shop -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title" id="nuevos">
							<h3 class="title">Nuevos Cupones</h3>
							<!-- <div class="section-nav">
								<ul class="section-tab-nav tab-nav">
									<li class="active"><a data-toggle="tab" href="#tab1">Comida</a></li>
									<li><a data-toggle="tab" href="#tab1">Electronico</a></li>
									<li><a data-toggle="tab" href="#tab1">Flores</a></li>
									<li><a data-toggle="tab" href="#tab1">Calzado</a></li>
								</ul>
							</div> -->
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										@foreach($cupones as $listCupones)
										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="/images/Images_cupones/{{ $listCupones->imagen }}" alt="">
												<div class="product-label">
													<span class="new">Nuevo</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">{{ $listCupones->titulo }}</p>
												<h3 class="product-name"><a href="#">{{ $listCupones->descripcion }}</a></h3>
												<!-- 
												<h4 class="product-price">$980.00 <del class="product-old-price">$990.00</del></h4> -->
												<!-- <div class="product-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</div> -->
												<div class="product-btns">
													<!-- <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button> -->
													<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">Canjeado {{ $listCupones->redimido }}</span></button>
													<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">Visto {{ $listCupones->visto }}</span></button>
												</div>
											</div>
											<div class="add-to-cart">
												<form class="form" method="POST" action="/canjear">
													{{ csrf_field() }}
													<div class="add-to-cart">
														<input type="hidden" name="tipo" value="1">
														<input type="hidden" name="cupon" value="{{ $listCupones->idcupon }}">
														<button class="add-to-cart-btn"><i class="fa fa-plus-square"></i> Canjear</button>
													</div>
												</form>
											</div>
										</div>
										<!-- /product -->
										@endforeach
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<div class="modal fade" id="myModalExito" tabindex="-1" role="dialog" > 
			<div class="modal-dialog"> <!-- Modal content--> 
				<div class="modal-content"> 
					<div class="modal-header"> 
						<form class="form" method="POST" action="/enviarCupon" enctype="multipart/form-data">
							{{ csrf_field() }}
							<!-- NEWSLETTER -->
							<div id="newsletter" class="section">
								<!-- container -->
								@if(isset($cupon))
								<div class="">
									<!-- row -->
									<div class="row">

										<div style="text-align: center; background: rgba(255, 255, 255, 0.9); border: 2px solid #eee; border-radius: 5px; overflow: hidden; height: 250px; width: 100%;">	
											<div style="position: relative; overflow: hidden; padding: 30px; height: 100%; float: left; width: 40%;">
												<h1 style="display: inline-block; position: relative; font-size: 24px; color: #344055; margin: 0;">{{ $cupon->titulo }}</h1>
												<p style="font-size: 13px; color: #7d7d7d;">{{ $cupon->descripcion }}</p>

											</div>
											<div style="display: inline-block; position: relative; overflow: hidden; height: 100%; float: right; width: 50%;">
												<img src="/images/Images_cupones/{{ $cupon->imagen }}" width="100%" height="100%">
											</div>
										</div>

										<div class="col-md-12">
											<div class="newsletter">
												<p>Enviar<strong>Cupon</strong> a mi correo</p>
												<div class="add-to-cart">
													<form class="form" method="POST" action="/enviarCupon">
														{{ csrf_field() }}
														<div class="row">
															
														</div>
														<div class="add-to-cart">
															<input type="hidden" name="idcupon" value="{{$cupon->idcupon}}">
															<input class="input" type="email" name="mail" placeholder="Ingrese su corre">
															<button class="add-to-cart-btn" style="background: #4c4092; border-radius: 5px; border-color: none; color:white; font-weight: bold; padding: 2%; width: 100px; margin-top: 10px;"> Enviar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<!-- /row -->
								</div>
								@endif
								<!-- /container -->
							</div>
							<!-- /NEWSLETTER -->
						</form>         
					</div> 
				</div> 
			</div> 
		</div>

		<!-- HOT DEAL SECTION -->
		<div id="hot-deal" class="section">
			<!-- container -->
			<div class="container">
			</div>
			<!-- /container -->
		</div>
		<!-- /HOT DEAL SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title" id="comercios">
							<h3 class="title">Comercios</h3>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab2" class="tab-pane fade in active">
									<div class="products-slick" data-nav="#slick-nav-2">
										<!-- product -->
										@foreach($comercios as $list)
										<!-- product -->
										<div class="product">
											<div class="product-img">
												<img src="/images/Images_comercio/{{ $list->imagen }}" alt="">
											</div>
											<div class="product-body">
												<p class="product-category">{{ $list->titulo }}</p>
												<h3 class="product-name"><a href="#">{{ $list->descripcion }}</a></h3>
												<div class="product-btns">
												</div>
											</div>
											<div class="add-to-cart">
													<div class="add-to-cart">
														<a href="{{ url('cuponesComercio', ['id' => $list->idcomercio]) }}">
														<button class="add-to-cart-btn"><i class="fa fa-plus-square"></i> Ver Cupones</button></a>
													</div>
											</div>
										</div>
										<!-- /product -->
										@endforeach
										<!-- /product -->
									</div>
									<div id="slick-nav-2" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- /Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Mas vistos</h4>
							<div class="section-nav">
								<div id="slick-nav-3" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-3">
							
								@if(count($vistos))
								@foreach($vistos as $list)
								<!-- product widget -->
								<div>
								<div class="product-widget">
									<div class="product-img">
										<img src="/images/Images_cupones/{{ $list->imagen }}" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">{{$list->categoria->titulo}}</p>
										<h3 class="product-name"><a href="#">{{$list->descripcion}}</a></h3>
									</div>
								</div>
								</div>
								@endforeach
								@endif
								<!-- /product widget -->

								
							

							
						</div>
					</div>

					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Nuevos</h4>
							<div class="section-nav">
								<div id="slick-nav-4" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-4">
							@if(count($cupones))
								@foreach($cupones as $list)
								<!-- product widget -->
								<div>
								<div class="product-widget">
									<div class="product-img">
										<img src="/images/Images_cupones/{{ $list->imagen }}" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">{{$list->categoria->titulo}}</p>
										<h3 class="product-name"><a href="#">{{$list->descripcion}}</a></h3>
									</div>
								</div>
								</div>
								@endforeach
								@endif
						</div>
					</div>

					<div class="clearfix visible-sm visible-xs"></div>

					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Top Flores</h4>
							<div class="section-nav">
								<div id="slick-nav-5" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-5">
							@if(count($cupones))
								@foreach($cupones as $list)
								<!-- product widget -->
								<div>
								<div class="product-widget">
									<div class="product-img">
										<img src="/images/Images_cupones/{{ $list->imagen }}" alt="">
									</div>
									<div class="product-body">
										<p class="product-category">{{$list->categoria->titulo}}</p>
										<h3 class="product-name"><a href="#">{{$list->descripcion}}</a></h3>
									</div>
								</div>
								</div>
								@endforeach
								@endif
						</div>
					</div>

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<!-- <div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Sign Up for the <strong>NEWSLETTER</strong></p>
							<form>
								<input class="input" type="email" placeholder="Enter Your Email">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-pinterest"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div> -->
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

		@include('pagina/footer')
		

		<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript">
		   $(document).ready(function()
		   {	
		   		<?php if (isset($canjear)) { ?>
		      		$("#myModalExito").modal("show");
		      	<?php } ?>
		   });
		</script>

		@include('pagina/scripts')


	</body>
	</html>
