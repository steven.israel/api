<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

<!-- Bootstrap -->
<link type="text/css" rel="stylesheet" href="{{ asset('/css/page/css/bootstrap.min.css')}}" />

<!-- Slick -->
<link type="text/css" rel="stylesheet" href="{{ asset('/css/page/css/slick.css')}}" />
<link type="text/css" rel="stylesheet" href="{{ asset('/css/page/css/slick-theme.css')}}" />

<!-- nouislider -->
<link type="text/css" rel="stylesheet" href="{{ asset('/css/page/css/nouislider.min.css')}}" />
<link type="text/css" rel="stylesheet" href="/css/page/css/nouislider.min.css"/>
<!-- Font Awesome Icon -->

<link type="text/css" rel="stylesheet" href="{{ asset('/css/page/css/font-awesome.min.css')}}" />
<!-- Custom stlylesheet -->

<link type="text/css" rel="stylesheet" href="{{ asset('/css/page/css/style.css')}}" />

<link rel="shortcut icon" href="{{ asset('/img/logo.png')}}">

