<!-- BEGIN BASE-->
<div id="base">

	<!-- BEGIN OFFCANVAS LEFT -->
	<div class="offcanvas">
	</div><!--end .offcanvas-->
	<!-- END OFFCANVAS LEFT -->

	<!-- BEGIN CONTENT-->
	<div id="content">
		<section>
			<div class="section-body contain-lg">

				<!-- BEGIN INTRO -->
				<div class="row">
					<div class="col-lg-12" align="">
						<h1 class="text-primary">COMERCIOS</h1>
					</div><!--end .col -->

				</div><!--end .row -->
				<!-- END INTRO -->

				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))

					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
					@endforeach
				</div> <!-- end .flash-message -->

				<div class="row">
					<!-- BEGIN LAYOUT JUSTIFIED -->
					<div class="col-md-12">
						<div class="card">
							<div class="card-head">
								<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
									<li id="hi2" class="active"><a href="#second4">CREAR COMERCIO</a></li>
								</ul>
							</div><!--end .card-head -->
							<div class="card-body tab-content">
								<div class="tab-pane " id="first4">
									
								</div>

								<div class="tab-pane active" id="second4">
									<!-- BEGIN VERTICAL FORM FLOATING LABELS -->
									<div class="row">
										<div class="col-lg-offset-0 col-md-12">
											<form class="form" method="POST" action="/actualizar.comercio/{{ $comercio->idcomercio }}" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="">
													<div class="card-head style-primary">
														<header>INGRESE LA INFORMACIÓN DE LA CATEGORÍA</header>
													</div>
													<div class="card-body floating-label">
														<input type="hidden" name="lat" id="lat">
														<input type="hidden" name="lon" id="lon">
														<br/>
														<?php
															$codigo = substr(strtoupper(sha1(time())), 0, 6);
														?>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $comercio->codigo }}" name="codigo" id="codigo" >
																	<label for="codigo">CODIGO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $comercio->titulo }}" name="titulo" id="titulo">
																	<label for="titulo">TITULO</label>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<input type="email" class="form-control" value="{{ $comercio->email }}" name="email" id="email">
																	<label for="email">CORREO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $comercio->telefono }}" name="telefono" id="telefono">
																	<label for="telefono">TELEFONO</label>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="descripcion">DESCRIPCIÓN</label>
																	<textarea name="descripcion" style="width: 100%; margin-top: 5px">{{ $comercio->descripcion }}</textarea>
																</div>
															</div>
															<div class="col-sm-12">
																<div id="map"></div>
															</div>
															<hr>
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="icono" class="lb1">ADJUNTAR LOGO 
																		<i class="fa fa-upload"></i>
																	</label>
																	<span><input type="file" onchange="cambio(1)" name="icono" id="icono"/></span>

																	<div class="col-sm-6"><span id="file-icono"></span></div>
																</div>
															</div><br><br>
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="img" class="lb2">ADJUNTAR IMAGEN 
																		<i class="fa fa-upload"></i>
																	</label>
																	<span><input type="file" onchange="cambio(2)" name="img" id="img"/></span>

																	<div class="col-sm-6"><span id="file-img"></span></div>
																</div>
															</div>
														</div>

													</div><!--end .card-body -->
													<div class="card-actionbar">
														<div class="card-actionbar-row">
															<button type="submit" class="btn btn-flat btn-primary ink-reaction">MODIFICAR COMERCIO</button>
														</div>
													</div>
												</div><!--end .card -->
											</form>
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END VERTICAL FORM FLOATING LABELS -->
								</div>

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<!-- END LAYOUT JUSTIFIED -->

				</div><!--end .row -->
			</section>
		</div><!--end #content-->
		<!-- END CONTENT -->


	</div><!--end #base-->
	<!-- END BASE -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	@if (count($errors) > 0)				
	<script type="text/javascript">
		$("#hi1").removeClass("active");
		$("#hi2").addClass("active");
		$("#first4").removeClass("active");
		$("#second4").addClass("active");
	</script>
	@endif
	<script type="text/javascript">
		var map;
		var zoom = 10;
		var latitud = <?=$comercio->latitud ?>;
		var longitud = <?=$comercio->longitud ?>;

		function cambio(tipo){
			if (tipo == 1) {
				$('.lb1').css({"background-color":"black"});
				var file = $('#icono')[0].files[0];
				$('#file-icono').text(file.name);
			}else{
				$('.lb2').css({"background-color":"black"});
				var file = $('#img')[0].files[0];
				$('#file-img').text(file.name);
			}
		}

		function initMap() {

			$("#lat").val(latitud);
			$("#lon").val(longitud);

			var position = {lat: latitud, lng: longitud };
	        // Styles a map in night mode.
	        map = new google.maps.Map(document.getElementById('map'), {
	          center: position,
	          zoom: zoom,
	          styles: [
	            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
	            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
	            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
	            {
	              featureType: 'administrative.locality',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#d59563'}]
	            },
	            {
	              featureType: 'poi',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#d59563'}]
	            },
	            {
	              featureType: 'poi.park',
	              elementType: 'geometry',
	              stylers: [{color: '#263c3f'}]
	            },
	            {
	              featureType: 'poi.park',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#6b9a76'}]
	            },
	            {
	              featureType: 'road',
	              elementType: 'geometry',
	              stylers: [{color: '#38414e'}]
	            },
	            {
	              featureType: 'road',
	              elementType: 'geometry.stroke',
	              stylers: [{color: '#212a37'}]
	            },
	            {
	              featureType: 'road',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#9ca5b3'}]
	            },
	            {
	              featureType: 'road.highway',
	              elementType: 'geometry',
	              stylers: [{color: '#746855'}]
	            },
	            {
	              featureType: 'road.highway',
	              elementType: 'geometry.stroke',
	              stylers: [{color: '#1f2835'}]
	            },
	            {
	              featureType: 'road.highway',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#f3d19c'}]
	            },
	            {
	              featureType: 'transit',
	              elementType: 'geometry',
	              stylers: [{color: '#2f3948'}]
	            },
	            {
	              featureType: 'transit.station',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#d59563'}]
	            },
	            {
	              featureType: 'water',
	              elementType: 'geometry',
	              stylers: [{color: '#17263c'}]
	            },
	            {
	              featureType: 'water',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#515c6d'}]
	            },
	            {
	              featureType: 'water',
	              elementType: 'labels.text.stroke',
	              stylers: [{color: '#17263c'}]
	            }
	          ]
	        });

	        var marker = new google.maps.Marker({
			    position: position,
			    map: map,
			    draggable: true,
			    title: 'Hello World!'
			});

			marker.addListener( 'dragend', function (event)
		    {
		        $("#lat").val(this.getPosition().lat());
		        $("#lon").val(this.getPosition().lng());
		    });

			marker.setMap(map);

			//geoposicion();
      	}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEUar6Zb3OxUF0gqffyatAkbEns33j9pk&callback=initMap"
    async defer></script>
