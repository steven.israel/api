<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard GeoCupon</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')
	</head>
	<body class="menubar-hoverable header-fixed ">
		@include('layout/navbar')
		<!-- Body dashboard -->
		<!-- BEGIN BASE-->
		<div id="base">
			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->
			<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
					<div class="section-body">
						<div class="row">
							<div class="col-md-12">
                                <h2>Listado de usuarios segun los cupones redimidos</h2>
                                <hr>
								<div class="table-responsive">
									@if($datos)
									<table id="table" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Usuarios</th>
												<th>Edad</th>
												<th>Sexo</th>
												<th>Cupones redimidos</th>
												<th>Detalles</th>
											</tr>
										</thead>
										<tbody>
											@foreach($datos as $list)
											<tr class="gradeU">
												<td>{{ $list->nombre }} {{ $list->apellido }}</td>
												<td>{{ $list->edad }}</td>
												<td>{{ $list->sexo }}</td>
												<td>{{ $list->cupones_redimidos() }}</td>
												<td>
													<a href="{{ url('ver.cupones', ['id' => $list->idusuario]) }}" title="Ver cupones">
														<i class="md md-remove-red-eye"></i> 
													</a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
									@endif
								</div>
								<hr>
                                {{ $datos->links() }}
							</div>
						</div>					
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->
		</div><!--end #base-->
		<!-- END BASE -->
		<!-- end body dashboard -->
		@include('layout/menu')
		@include('layout/scripts')

	</body>
</html>