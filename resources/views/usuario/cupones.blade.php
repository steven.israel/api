<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard GeoCupon</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')
	</head>
	<body class="menubar-hoverable header-fixed ">
		@include('layout/navbar')
		<!-- Body dashboard -->
		<!-- BEGIN BASE-->
		<div id="base">
			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->
			<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
					<div class="section-body">
						<div class="row">
							<div class="col-md-12">
                                <h2>Listado de cupones redimidos usuario: {{ $user->nombre }} {{ $user->apellido }}</h2>
                                <hr>
								<div class="table-responsive">
									@if($datos)
									<table id="table" class="table table-striped table-hover">
										<thead>
											<tr>
												<th>LOGO</th>
												<th>COMERCIO</th>
												<th>CATEGORIA</th>
												<th>TITULO</th>
												<th>DESCRIPCIÓN</th>
												<th>FECHA REDIMIDO</th>
											</tr>
										</thead>
										<tbody>
											@foreach($datos as $list)
											<tr class="gradeU">
												<td width="5%">
													<img src="/images/Images_cupones/{{ $list->cupones->imagen }}" width="30px" alt="avatar">
												</td>
												<td>{{ $list->cupones->comercio->titulo }}</td>
												<td>{{ $list->cupones->categoria->titulo }}</td>
												<td>{{ $list->cupones->titulo }}</td>
												<td>{{ $list->cupones->descripcion }}</td>
												<td>{{ $list->created_at }}</td>
											</tr>
											@endforeach
										</tbody>
									</table>
									@endif
                                </div>
                                {{ $datos->links() }}
							</div>
						</div>					
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->
		</div><!--end #base-->
		<!-- END BASE -->
		<!-- end body dashboard -->
		@include('layout/menu')
		@include('layout/scripts')

	</body>
</html>