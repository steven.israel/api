<!-- BEGIN BASE-->
<div id="base">

	<!-- BEGIN OFFCANVAS LEFT -->
	<div class="offcanvas">
	</div><!--end .offcanvas-->
	<!-- END OFFCANVAS LEFT -->

	<!-- BEGIN CONTENT-->
	<div id="content">
		<section>
			<div class="section-body contain-lg">

				<!-- BEGIN INTRO -->
				<div class="row">
					<div class="col-lg-12" align="">
						<h1 class="text-primary">CATEGORÍAS</h1>
					</div><!--end .col -->

				</div><!--end .row -->
				<!-- END INTRO -->

				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))

					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
					@endforeach
				</div> <!-- end .flash-message -->

				<div class="row">
					<!-- BEGIN LAYOUT JUSTIFIED -->
					<div class="col-md-12">
						<div class="card">
							<div class="card-head">
								<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
									<li id="hi2" class="active"><a href="#second4">MODIFICAR CATEGORÍA</a></li>
								</ul>
							</div><!--end .card-head -->
							<div class="card-body tab-content">
								<div class="tab-pane " id="first4">
									
								</div>

								<div class="tab-pane active" id="second4">
									<!-- BEGIN VERTICAL FORM FLOATING LABELS -->
									<div class="row">
										<div class="col-lg-offset-0 col-md-12">
											<form class="form" method="POST" action="/actualizar.categoria/{{ $categoria->idcategoria }}" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="">
													<div class="card-head style-primary">
														<header>INGRESE LA INFORMACIÓN DE LA CATEGORÍA</header>
													</div>
													<div class="card-body floating-label">

														<br/>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $categoria->titulo }}" name="titulo" id="titulo">
																	<label for="titulo">TITULO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $categoria->color }}" name="color" id="titulo">
																	<label for="titulo">TITULO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="icono" class="lb1">ADJUNTAR LOGO 
																		<i class="fa fa-upload"></i>
																	</label>
																	<span><input type="file" onchange="cambio(1)" name="icono" id="icono"/></span>

																	<div class="col-sm-6"><span id="file-icono"></span></div>
																</div>
															</div>
														</div>

													</div><!--end .card-body -->
													<div class="card-actionbar">
														<div class="card-actionbar-row">
															<button type="submit" class="btn btn-flat btn-primary ink-reaction">ACTUALIZAR CATEGORÍA</button>
														</div>
													</div>
												</div><!--end .card -->
											</form>
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END VERTICAL FORM FLOATING LABELS -->
								</div>

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<!-- END LAYOUT JUSTIFIED -->

				</div><!--end .row -->
			</section>
		</div><!--end #content-->
		<!-- END CONTENT -->


	</div><!--end #base-->
	<!-- END BASE -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	@if (count($errors) > 0)				
	<script type="text/javascript">
		$("#hi1").removeClass("active");
		$("#hi2").addClass("active");
		$("#first4").removeClass("active");
		$("#second4").addClass("active");
	</script>
	@endif
	<script type="text/javascript">

		function cambio(tipo){
			if (tipo == 1) {
				$('.lb1').css({"background-color":"black"});
				var file = $('#icono')[0].files[0];
				$('#file-icono').text(file.name);
			}else{
				$('.lb2').css({"background-color":"black"});
				var file = $('#icono_foto')[0].files[0];
				$('#file-icono_foto').text(file.name);
			}
		}
	</script>