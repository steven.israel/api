<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Categorías</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')
		
	</head>
	<body class="menubar-hoverable header-fixed menubar-pin ">

		@include('../layout/navbar');

		@if($editar)
			@include('Categoria/editar_categoria')
		@else
			@include('Categoria/index')
		@endif

		@include('../layout/menu')
	
		@include('../layout/scripts')

	</body>
</html>