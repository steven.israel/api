<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard GeoCupon</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')
	</head>
	<body class="menubar-hoverable header-fixed ">
		@include('layout/navbar')
		<!-- Body dashboard -->
		<!-- BEGIN BASE-->
		<div id="base">
			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->
			<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
						<div class="row">
							<div class="col-md-12"><h2>CATEGORÍAS</h2></div>
							@if($categorias)
								@foreach($categorias as $list)
									<a href="{{ url('cupon', ['id' => $list->idcategoria]) }}">
										<div class="col-md-2" align="center">
											<div class="card" style="background: {{ $list->color }};">
												<img src="images/images_categoria/{{ $list->logo }}" width="180px" alt="categoria">
											</div>
										</div>
									</a>
								@endforeach
							@endif
						</div>						
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->
		</div><!--end #base-->
		<!-- END BASE -->
		<!-- end body dashboard -->
		@include('layout/menu')
		@include('layout/scripts')
	</body>
</html>