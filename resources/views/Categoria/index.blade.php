<!-- BEGIN BASE-->
<div id="base">

	<!-- BEGIN OFFCANVAS LEFT -->
	<div class="offcanvas">
	</div><!--end .offcanvas-->
	<!-- END OFFCANVAS LEFT -->

	<!-- BEGIN CONTENT-->
	<div id="content">
		<section>
			<div class="section-body contain-lg">

				<!-- BEGIN INTRO -->
				<div class="row">
					<div class="col-lg-12" align="">
						<h1 class="text-primary">CATEGORÍAS</h1>
					</div><!--end .col -->

				</div><!--end .row -->
				<!-- END INTRO -->
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))

					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
					@endforeach
				</div> <!-- end .flash-message -->

				<div class="row">
					<!-- BEGIN LAYOUT JUSTIFIED -->
					<div class="col-md-12">
						<div class="card">
							<div class="card-head">
								<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
									<li id="hi1" class="active"><a href="#first4">LISTA DE CATEGORÍAS</a></li>
									<li id="hi2" ><a href="#second4">CREAR CATEGORÍA</a></li>
								</ul>
							</div><!--end .card-head -->
							<div class="card-body tab-content">
								<div class="tab-pane active" id="first4">
									<!-- BEGIN DATETABLE-->
									<div class="row">
										<div class="col-lg-12">
											<div class="table-responsive">
												@if($datos)
												<table id="datatable1" class="table table-striped table-hover">
													<thead>
														<tr>
															<th>LOGO</th>
															<th>TITULO</th>
															<th>COLOR</th>
															<th>OPCIONES</th>
														</tr>
													</thead>
													<tbody>
														@foreach($datos as $list)
														<tr class="gradeU">
															<td width="10%">
																<img src="images/images_categoria/{{ $list->logo }}" width="30px" alt="avatar">
															</td>
															<td>{{ $list->titulo }}</td>
															<td>{{ $list->color }}</td>
															<td>
																<span >
																	<a href="{{ url('editar.categoria', ['id' => $list->idcategoria]) }}" title="Modificar">
																		<i class="md md-mode-edit"></i> 
																	</a>
																	<a href="{{ url('eliminar.categoria', ['id' => $list->idcategoria]) }}" title="Eliminar">
																		<i class="md md-delete"></i> 
																	</a>
																</span>
															</td>
														</tr>
														@endforeach
													</tbody>
												</table>
												@endif
											</div><!--end .table-responsive -->
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END DATETABLE -->
								</div>

								<div class="tab-pane" id="second4">
									<!-- BEGIN VERTICAL FORM FLOATING LABELS -->
									<div class="row">
										<div class="col-lg-offset-0 col-md-12">
											<form class="form" method="POST" action="/crear.categoria" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="">
													<div class="card-head style-primary">
														<header>INGRESE LA INFORMACIÓN DE LA CATEGORÍA</header>
													</div>
													<div class="card-body floating-label">

														<br/>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" name="titulo" id="titulo">
																	<label for="titulo">TITULO</label>
																	<p style="color: red;">{{ $errors->first('titulo') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="color" class="form-control" name="color" id="color">
																	<label for="color">COLOR</label>
																	<p style="color: red;">{{ $errors->first('color') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<p style="color: red;">{{ $errors->first('icono') }}</p>
																	<label for="icono" class="lb1">ADJUNTAR LOGO 
																		<i class="fa fa-upload"></i>
																	</label>
																	<span><input type="file" onchange="cambio(1)" name="icono" id="icono"/></span>

																	<div class="col-sm-6"><span id="file-icono"></span></div>
																</div>
															</div>
														</div>

													</div><!--end .card-body -->
													<div class="card-actionbar">
														<div class="card-actionbar-row">
															<button type="submit" class="btn btn-flat btn-primary ink-reaction">CREAR CATEGORÍA</button>
														</div>
													</div>
												</div><!--end .card -->
											</form>
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END VERTICAL FORM FLOATING LABELS -->
								</div>

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<!-- END LAYOUT JUSTIFIED -->

				</div><!--end .row -->
			</section>
		</div><!--end #content-->
		<!-- END CONTENT -->


	</div><!--end #base-->
	<!-- END BASE -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	@if (count($errors) > 0)				
	<script type="text/javascript">
		$("#hi1").removeClass("active");
		$("#hi2").addClass("active");
		$("#first4").removeClass("active");
		$("#second4").addClass("active");
	</script>
	@endif
	<script type="text/javascript">

		function cambio(tipo){
			if (tipo == 1) {
				$('.lb1').css({"background-color":"black"});
				var file = $('#icono')[0].files[0];
				$('#file-icono').text(file.name);
			}else{
				$('.lb2').css({"background-color":"black"});
				var file = $('#icono_foto')[0].files[0];
				$('#file-icono_foto').text(file.name);
			}
		}
	</script>