<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard Smart912</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')

	</head>
	<body class="menubar-hoverable header-fixed ">
		

		@include('layout/navbar')

		<!-- Body dashboard -->

			<!-- BEGIN BASE-->
		<div id="base">

			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->

			<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
					<div class="section-body">
						<div class="row">
							<div class="col-md-12"><a href="{{ url('dashboard') }}" id="back"><span class="md md-arrow-back"></a></i><h2>CUPONES CATEGORÍA {{ $categoria }}</h2></div>
							@if(count($cupones) > 0)
							@foreach($cupones as $list)
							<!-- BEGIN ALERT - REVENUE -->
							<div class="col-md-3 col-sm-6">
								<div class="card">
									<div class="card-body no-padding">
										<div class="alert alert-callout alert-info no-margin">
											<div width="100%">
											<strong class="pull-right text-success text-lg">
												<img src="../images/Images_cupones/{{ $list->imagen }}" width="60px" alt="avatar">
											</strong>
											</div>
											<strong class="text-xl">{{ $list->titulo }}</strong><br/>
											<span class="opacity-50">{{ $list->descripcion }}</span>
											<div class="stick-bottom-left-right">
												<div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
											</div>
										</div>
									</div><!--end .card-body -->
								</div><!--end .card -->
							</div><!--end .col -->
							<!-- END ALERT - REVENUE -->
							@endforeach
							@else
								<article>
									<div class="col-md-12" align="center">
										<h2>No hay cupones para esta categoría</h2>
									</div>
								</article>
							@endif

						</div><!--end .row -->						
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->

		</div><!--end #base-->
	<!-- END BASE -->

		<!-- end body dashboard -->
		
		@include('layout/menu')

		@include('layout/scripts')

	</body>
</html>