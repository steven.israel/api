<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Cupones</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')
		
	</head>
	<body class="menubar-hoverable header-fixed menubar-pin ">

		@include('../layout/navbar');

		<!-- BEGIN BASE-->
<div id="base">

	<!-- BEGIN OFFCANVAS LEFT -->
	<div class="offcanvas">
	</div><!--end .offcanvas-->
	<!-- END OFFCANVAS LEFT -->

	<!-- BEGIN CONTENT-->
	<div id="content">
		<section>
			<div class="section-header">
				<ol class="breadcrumb">
					<li><a href="{{ url('cupones') }}">Lista de cupones</a></li>
					<li class="active">Vista previa</li>
				</ol>
			</div>
			
			<div class="section-body">
						<div class="card">
							<!-- BEGIN CONTACT DETAILS -->
							<div class="card-tiles">
								<div class="hbox-md col-md-12">
									<div class="hbox-column col-md-9">
										<div class="row">

											<!-- BEGIN CONTACTS MAIN CONTENT -->
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="margin-bottom-xxl">
													<div class="pull-left width-3 clearfix hidden-xs">
														<img class="img-circle size-2" src="/images/Images_cupones/{{ $cupon->imagen }}" alt="">
													</div>
													<h1 class="text-light no-margin">{{ $cupon->titulo }}</h1>
													<h5>
														Creado: {{ $cupon->created_at }}
													</h5>
												</div><!--end .margin-bottom-xxl -->
												<ul class="nav nav-tabs" data-toggle="tabs">
													<li class="active"><a href="#notes">Informacion del cupon</a></li>
												</ul>
												<div class="tab-content">

													<!-- BEGIN CONTACTS NOTES -->
													<div class="tab-pane active" id="notes">
														<div class="">
															<div class="col-md-12" align="">
																<h2>Descripcion:</h2>
																<p>{{ $cupon->descripcion }}</p>
															</div><!--end .col -->
																<div class="col-md-4">
																	<h2>Fecha de creacino</h2>
																	<p>{{ $cupon->fecha_creacion }}</p>
																</div>
																<div class="col-md-4">
																	<h2>Fecha de vencimiento</h2>
																	<p>{{ $cupon->fecha_vencimiento }}</p>
																</div>
																<div class="col-md-4">
																	<h2>Categoria</h2>
																	<p>{{ $cupon->categoria->titulo }}</p>
																</div>
														</div><!--end .list-results -->
													</div><!--end #notes -->
													<!-- END CONTACTS NOTES -->
											</div><!--end .tab-content -->
										</div><!--end .col -->
										<!-- END CONTACTS MAIN CONTENT -->

									</div><!--end .row -->
								</div><!--end .hbox-column -->

								<!-- BEGIN CONTACTS COMMON DETAILS -->
								<div class="hbox-column col-md-3 style-default-light">
									<div class="row">
										<div class="col-xs-12">
											<h4>INFORMACION DEL COMERCIO</h4>
											<br>
											<dl class="dl-horizontal dl-icon">
												<dt><span class="md md-account-balance md-lg opacity-50"></span></dt>
												<dd>
													<span class="opacity-50">Nombre</span><br>
													<span class="text-medium">{{ $cupon->comercio->titulo }}</span>
												</dd>
												<dt><span class="md md-email opacity-50"></span></dt>
												<dd>
													<span class="opacity-50">Correo</span><br>
													<span class="text-medium">{{ $cupon->comercio->email }}</span>
												</dd>
											</dl><!--end .dl-horizontal -->
											<br>
											<h4>Ubicacion del comercio</h4>
											<br>
											<dl class="dl-horizontal dl-icon">
												<dd class="full-width"><div id="map"></div></dd>
											</dl><!--end .dl-horizontal -->
										</div><!--end .col -->
									</div><!--end .row -->
								</div><!--end .hbox-column -->
								<!-- END CONTACTS COMMON DETAILS -->

							</div><!--end .hbox-md -->
						</div><!--end .card-tiles -->
						<!-- END CONTACT DETAILS -->

					</div><!--end .card -->
				</div>
		</section>	
	</div><!--end #content-->
		<!-- END CONTENT -->


	</div><!--end #base-->
	<!-- END BASE -->

	@include('../layout/menu')
	
	@include('../layout/scripts')
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEUar6Zb3OxUF0gqffyatAkbEns33j9pk"></script>
	<script type="text/javascript">

		var longitud = <?=$cupon->comercio->longitud?>;
		var latitud = <?=$cupon->comercio->latitud?>;
		var zoom = 10;
		var map;

		initMap();

		function initMap() {

			var position = {lat: latitud, lng: longitud };

			console.log(position);
	        // Styles a map in night mode.
	        var map = new google.maps.Map(document.getElementById('map'), {
	          center: position,
	          zoom: zoom,
	          styles: [
	            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
	            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
	            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
	            {
	              featureType: 'administrative.locality',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#d59563'}]
	            },
	            {
	              featureType: 'poi',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#d59563'}]
	            },
	            {
	              featureType: 'poi.park',
	              elementType: 'geometry',
	              stylers: [{color: '#263c3f'}]
	            },
	            {
	              featureType: 'poi.park',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#6b9a76'}]
	            },
	            {
	              featureType: 'road',
	              elementType: 'geometry',
	              stylers: [{color: '#38414e'}]
	            },
	            {
	              featureType: 'road',
	              elementType: 'geometry.stroke',
	              stylers: [{color: '#212a37'}]
	            },
	            {
	              featureType: 'road',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#9ca5b3'}]
	            },
	            {
	              featureType: 'road.highway',
	              elementType: 'geometry',
	              stylers: [{color: '#746855'}]
	            },
	            {
	              featureType: 'road.highway',
	              elementType: 'geometry.stroke',
	              stylers: [{color: '#1f2835'}]
	            },
	            {
	              featureType: 'road.highway',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#f3d19c'}]
	            },
	            {
	              featureType: 'transit',
	              elementType: 'geometry',
	              stylers: [{color: '#2f3948'}]
	            },
	            {
	              featureType: 'transit.station',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#d59563'}]
	            },
	            {
	              featureType: 'water',
	              elementType: 'geometry',
	              stylers: [{color: '#17263c'}]
	            },
	            {
	              featureType: 'water',
	              elementType: 'labels.text.fill',
	              stylers: [{color: '#515c6d'}]
	            },
	            {
	              featureType: 'water',
	              elementType: 'labels.text.stroke',
	              stylers: [{color: '#17263c'}]
	            }
	          ]
	        });

	        var marker = new google.maps.Marker({
			    position: position,
			    map: map
			});

			marker.setMap(map);
      	}

    </script>
	</body>
</html>
