<!-- BEGIN BASE-->
<div id="base">

	<!-- BEGIN OFFCANVAS LEFT -->
	<div class="offcanvas">
	</div><!--end .offcanvas-->
	<!-- END OFFCANVAS LEFT -->

	<!-- BEGIN CONTENT-->
	<div id="content">
		<section>
			<div class="section-body contain-lg">

				<!-- BEGIN INTRO -->
				<div class="row">
					<div class="col-lg-12" align="">
						<h1 class="text-primary">CUPONES</h1>
					</div><!--end .col -->

				</div><!--end .row -->
				<!-- END INTRO -->
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))

					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
					@endforeach
				</div> <!-- end .flash-message -->

				<div class="row">
					<!-- BEGIN LAYOUT JUSTIFIED -->
					<div class="col-md-12">
						<div class="card">
							<div class="card-head">
								<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
									<li id="hi1" class="active"><a href="#first4">LISTA DE CUPONES</a></li>
									<li id="hi2" ><a href="#second4">CREAR CUPON</a></li>
								</ul>
							</div><!--end .card-head -->
							<div class="card-body tab-content">
								<div class="tab-pane active" id="first4">
									<!-- BEGIN DATETABLE-->
									<div class="row">
										<div class="col-lg-12">
											<div class="table-responsive">
												@if($datos)
												<table id="datatable1" class="table table-striped table-hover">
													<thead>
														<tr>
															<th>LOGO</th>
															<th>COMERCIO</th>
															<th>CATEGORIA</th>
															<th>TITULO</th>
															<th>DESCRIPCIÓN</th>
															<th>FECHA DE CREACIÓN</th>
															<th>FECHA DE VENCIMIENTO</th>
															<th>REDIMIDO</th>
															<th>LIMITE</th>
															<th>VISTAS</th>
															<th>ESTADO</th>
															<th>OPCIONES</th>
														</tr>
													</thead>
													<tbody>
														@foreach($datos as $list)
														<tr class="gradeU">
															<td width="5%">
																<img src="/images/Images_cupones/{{ $list->imagen }}" width="30px" alt="avatar">
															</td>
															<td>{{ $list->comercio->titulo }}</td>
															<td>{{ $list->categoria->titulo }}</td>
															<td>{{ $list->titulo }}</td>
															<td>{{ $list->descripcion }}</td>
															<td>{{ $list->fecha_creacion }}</td>
															<td>{{ $list->fecha_vencimiento }}</td>
															<td>{{ $list->redimido }}</td>
															<td>{{ $list->limite }}</td>
															<td>{{ $list->visto }}</td>
															<td>
																@if($list->estado)
																 	Publicado
																@else
																	Sin publicar
																@endif
															</td>
															<td width="5%">
																<span >
																	<a href="{{ url('vista.cupon', ['id' => $list->idcupon]) }}" title="Modificar">
																		<i class="md md-remove-red-eye"></i> 
																	</a>
																	<a href="{{ url('editar.cupon', ['id' => $list->idcupon]) }}" title="Modificar">
																		<i class="md md-mode-edit"></i> 
																	</a>
																	<a href="{{ url('eliminar.cupon', ['id' => $list->idcupon]) }}" title="Eliminar">
																		<i class="md md-delete"></i> 
																	</a>
																</span>
															</td>
														</tr>
														@endforeach
													</tbody>
												</table>
												@endif
											</div><!--end .table-responsive -->
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END DATETABLE -->
								</div>

								<div class="tab-pane" id="second4">
									<!-- BEGIN VERTICAL FORM FLOATING LABELS -->
									<div class="row">
										<div class="col-lg-offset-0 col-md-12">
											<form class="form" method="POST" action="/crear.cupon" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="">
													<div class="card-head style-primary">
														<header>INGRESE LA INFORMACIÓN DE EL CUPON</header>
													</div>
													<div class="card-body floating-label">
														<?php
															$codigo = substr(strtoupper(sha1(time())), 0, 6);
														?>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="<?php echo $codigo?>" name="codigo" id="codigo" >
																	<label for="codigo">CODIGO</label>
																	<p style="color: red;">{{ $errors->first('codigo') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" name="titulo" id="titulo">
																	<label for="titulo">TITULO</label>
																	<p style="color: red;">{{ $errors->first('titulo') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<select id="comercio" name="comercio" class="form-control">
																		@foreach($comercio as $list)
																			<option value="{{ $list->idcomercio }}">{{ $list->titulo }}</option>
																		@endforeach
																	</select>
																	<label for="comercio">COMERCIO</label>
																	<p style="color: red;">{{ $errors->first('comercio') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<select id="categoria" name="categoria" class="form-control">
																		@foreach($categoria as $list)
																			<option value="{{ $list->idcategoria }}">{{ $list->titulo }}</option>
																		@endforeach
																	</select>
																	<label for="categoria">CATEGORIA</label>
																	<p style="color: red;">{{ $errors->first('categoria') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="date" class="form-control" value="<?=date('Y-m-d')?>" name="fecha_crea" id="fecha_crea">
																	<label for="fecha_crea">FECHA DE CREACIÓN</label>
																	<p style="color: red;">{{ $errors->first('fecha_crea') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="date" class="form-control" value="<?=date('Y-m-d')?>" name="fecha_ven" id="fecha_ven">
																	<label for="fecha_ven">FECHA DE VENCIMIENTO</label>
																	<p style="color: red;">{{ $errors->first('fecha_ven') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<select id="estado" name="estado" class="form-control">
																		<option value="0">Sin publicar</option>
																		<option value="1">Publicar</option>
																	</select>
																	<label for="estado">Estado de publicación</label>
																	<p style="color: red;">{{ $errors->first('estado') }}</p>
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="descripcion">DESCRIPCIÓN</label>
																	<textarea name="descripcion" style="width: 100%; margin-top: 5px"></textarea>
																	<p style="color: red;">{{ $errors->first('descripcion') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="" name="limite" id="limite" >
																	<label for="limite">LIMITE</label>
																	<p style="color: red;">{{ $errors->first('limite') }}</p>
																</div>
															</div>
															<div class="col-sm-6">
																<p style="color: red;">{{ $errors->first('img') }}</p>
																<div class="form-group">
																	<label for="img" class="lb2">ADJUNTAR IMAGEN 
																		<i class="fa fa-upload"></i>
																	</label>
																	<span><input type="file" onchange="cambio(2)" name="img" id="img"/></span>

																	<div class="col-sm-6"><span id="file-img"></span></div>
																</div>
															</div>
															
														</div>

													</div><!--end .card-body -->
													<div class="card-actionbar">
														<div class="card-actionbar-row">
															<button type="submit" class="btn btn-flat btn-primary ink-reaction">CREAR CUPON</button>
														</div>
													</div>
												</div><!--end .card -->
											</form>
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END VERTICAL FORM FLOATING LABELS -->
								</div>

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<!-- END LAYOUT JUSTIFIED -->

				</div><!--end .row -->
			</section>
		</div><!--end #content-->
		<!-- END CONTENT -->


	</div><!--end #base-->
	<!-- END BASE -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	@if (count($errors) > 0)				
	<script type="text/javascript">
		$("#hi1").removeClass("active");
		$("#hi2").addClass("active");
		$("#first4").removeClass("active");
		$("#second4").addClass("active");
	</script>
	@endif
	<script type="text/javascript">

		var lon = 0;
		var lat = 0;

		function cambio(tipo){
			if (tipo == 1) {
				$('.lb1').css({"background-color":"black"});
				var file = $('#icono')[0].files[0];
				$('#file-icono').text(file.name);
			}else{
				$('.lb2').css({"background-color":"black"});
				var file = $('#img')[0].files[0];
				$('#file-img').text(file.name);
			}
		}

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFAaWjKr_2Hl2f-23n-u4MaKsUI1f8ROg"
    async defer></script>