<!-- BEGIN BASE-->
<div id="base">

	<!-- BEGIN OFFCANVAS LEFT -->
	<div class="offcanvas">
	</div><!--end .offcanvas-->
	<!-- END OFFCANVAS LEFT -->

	<!-- BEGIN CONTENT-->
	<div id="content">
		<section>
			<div class="section-body contain-lg">

				<!-- BEGIN INTRO -->
				<div class="row">
					<div class="col-lg-12" align="">
						<h1 class="text-primary">CUPONES</h1>
					</div><!--end .col -->

				</div><!--end .row -->
				<!-- END INTRO -->

				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))

					<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
					@endforeach
				</div> <!-- end .flash-message -->

				<div class="row">
					<!-- BEGIN LAYOUT JUSTIFIED -->
					<div class="col-md-12">
						<div class="card">
							<div class="card-head">
								<ul class="nav nav-tabs nav-justified" data-toggle="tabs">
									<li id="hi2" class="active"><a href="#second4">ACTUALIZAR CUPON</a></li>
								</ul>
							</div><!--end .card-head -->
							<div class="card-body tab-content">
								<div class="tab-pane" id="first4">
									
								</div>

								<div class="tab-pane active" id="second4">
									<!-- BEGIN VERTICAL FORM FLOATING LABELS -->
									<div class="row">
										<div class="col-lg-offset-0 col-md-12">
											<form class="form" method="POST" action="/actualizar.cupon/{{ $cupon[0]->idcupon }}" enctype="multipart/form-data">
												{{ csrf_field() }}
												<div class="">
													<div class="card-head style-primary">
														<header>INGRESE LA INFORMACIÓN DEL CUPON</header>
													</div>
													<div class="card-body floating-label">
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $cupon[0]->codigo }}" name="codigo" id="codigo" >
																	<label for="codigo">CODIGO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $cupon[0]->titulo }}" name="titulo" id="titulo">
																	<label for="titulo">TITULO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<select id="comercio" name="comercio" class="form-control">
																		@foreach($comercio as $list)
																			<option value="{{ $list->idcomercio }}" @if($cupon[0]->idcomercio == $list->idcomercio) selected @endif>{{ $list->titulo }}</option>
																		@endforeach
																	</select>
																	<label for="comercio">COMERCIO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<select id="categoria" name="categoria" class="form-control">
																		@foreach($categoria as $list)
																			<option value="{{ $list->idcategoria }}" @if($cupon[0]->idcategoria == $list->idcategoria) selected @endif>{{ $list->titulo }}</option>
																		@endforeach
																	</select>
																	<label for="categoria">CATEGORIA</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="date" class="form-control" value="<?=date('Y-m-d')?>" name="fecha_crea" id="fecha_crea">
																	<label for="fecha_crea">FECHA DE CREACIÓN</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="date" class="form-control" value="<?=date('Y-m-d')?>" name="fecha_ven" id="fecha_ven">
																	<label for="fecha_ven">FECHA DE VENCIMIENTO</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<select id="estado" name="estado" class="form-control">
																		<option value="0" @if($cupon[0]->estado == 0) selected @endif>Sin publicar</option>
																		<option value="1" @if($cupon[0]->estado == 1) selected @endif>Publicado</option>
																	</select>
																	<label for="estado">Estado de publicación</label>
																	<p style="color: red;">{{ $errors->first('estado') }}</p>
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="descripcion">DESCRIPCIÓN</label>
																	<textarea name="descripcion" style="width: 100%; margin-top: 5px">{{ $cupon[0]->descripcion }}</textarea>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<input type="text" class="form-control" value="{{ $cupon[0]->limite }}" name="limite" id="limite" >
																	<label for="limite">LIMITE</label>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group">
																	<label for="img" class="lb2">ADJUNTAR IMAGEN 
																		<i class="fa fa-upload"></i>
																	</label>
																	<span><input type="file" onchange="cambio(2)" name="img" id="img"/></span>

																	<div class="col-sm-6"><span id="file-img"></span></div>
																</div>
															</div>
															
														</div>

													</div><!--end .card-body -->
													<div class="card-actionbar">
														<div class="card-actionbar-row">
															<button type="submit" class="btn btn-flat btn-primary ink-reaction">ACTUALIZAR CUPON</button>
														</div>
													</div>
												</div><!--end .card -->
											</form>
										</div><!--end .col -->
									</div><!--end .row -->
									<!-- END VERTICAL FORM FLOATING LABELS -->
								</div>

							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<!-- END LAYOUT JUSTIFIED -->

				</div><!--end .row -->
			</section>
		</div><!--end #content-->
		<!-- END CONTENT -->


	</div><!--end #base-->
	<!-- END BASE -->
	<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
	@if (count($errors) > 0)				
	<script type="text/javascript">
		$("#hi1").removeClass("active");
		$("#hi2").addClass("active");
		$("#first4").removeClass("active");
		$("#second4").addClass("active");
	</script>
	@endif
	<script type="text/javascript">

		var lon = 0;
		var lat = 0;

		function cambio(tipo){
			if (tipo == 1) {
				$('.lb1').css({"background-color":"black"});
				var file = $('#icono')[0].files[0];
				$('#file-icono').text(file.name);
			}else{
				$('.lb2').css({"background-color":"black"});
				var file = $('#img')[0].files[0];
				$('#file-img').text(file.name);
			}
		}

		
    </script>