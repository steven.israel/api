<!DOCTYPE html>
<html lang="en">
	<head>
		<title class="favicon">Login</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.css')}} " />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/materialadmin.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/material-design-iconic-font.min.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{ asset('css/app.css')}}" />
		<!-- END STYLESHEETS -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
		<script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
		<![endif]-->
	</head>
	<!-- style="background-image: url('images/mapa.png'); width: 100%;" -->
	<body class="menubar-hoverable header-fixed " >

		<!-- BEGIN LOGIN SECTION -->
		<section class="section-account" style="padding-top: 100px;">

			<!-- <div class="img-backdrop" style="background-image: url('/images/img16.jpg')">				
			</div> -->
			<!-- <div class="spacer"></div>-->
			<div class="card contain-sm style-transparent">
			<div class="card-body">
				<div class="row">

					<div class="col-md-12">
					@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
					</div>
					
					<div class="col-sm-6 col-lg-offset-3" id="login-box" style="background-color: white; padding: 5%;">
						<div class="row">
							<div class="col-sm-12" align="center">
								<img class="img-mred" style="width: 70px"  src="images/logo.png">	
							</div>
						</div>
						<br/>
						<!-- <span class="text-lg text-bold text-primary">Login</span>
						 -->
						<form class="form floating-label" method="POST" action="{{ url('validarLogin') }}" accept-charset="utf-8">
							{{ csrf_field() }}
							<div class="form-group" align="center">
								<input type="text" class="form-control" id="email" name="email" value="">
								<label for="email">CORREO</label>
							</div>
							<div class="form-group" align="center">
								<input type="password" class="form-control" id="password" name="password">
								<label for="password">CONTRASEÑA</label>
							</div>
							<br/>
							<div class="row" align="center">
								<div class="col-md-12 text-center">
									<button class="btn ink-reaction btn-raised btn-lg btn-primary" type="submit">INICIAR SESIÓN</button>
								</div><!--end .col -->
							</div><!--end .row -->
						</form>
					</div><!--end .col -->
				</div><!--end .row -->
			</div><!--end .card-body -->
		</div><!--end .card -->

			
		</section>
		<!-- END LOGIN SECTION -->
		<div class="" align="center">
			<footer>Copyright * Geocupon</footer>
		</div>
		<!-- BEGIN JAVASCRIPT -->
		<script src="{{ asset('js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
		<script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
		<script src="{{ asset('js/libs/bootstrap/bootstrap.min.js')}}"></script>
		<script src="{{ asset('js/libs/spin.js/spin.min.js')}}"></script>
		<script src="{{ asset('js/libs/autosize/jquery.autosize.min.js')}}"></script>
		<script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
		<script src="{{ asset('js/core/source/App.js')}}"></script>
		<script src="{{ asset('js/core/source/AppNavigation.js')}}"></script>
		<script src="{{ asset('js/core/source/AppOffcanvas.js')}}"></script>
		<script src="{{ asset('js/core/source/AppCard.js')}}"></script>
		<script src="{{ asset('js/core/source/AppForm.js')}}"></script>
		<script src="{{ asset('js/core/source/AppNavSearch.js')}}"></script>
		<script src="{{ asset('js/core/source/AppVendor.js')}}"></script>
		<script src="{{ asset('js/core/demo/Demo.js')}}"></script>
		<!-- END JAVASCRIPT -->

	</body>
</html>
