<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Dashboard GeoCupon</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->
		@include('layout/links')
	</head>
	<body class="menubar-hoverable header-fixed ">
		@include('layout/navbar')
		<!-- Body dashboard -->
		<!-- BEGIN BASE-->
		<div id="base">
			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->
			<!-- BEGIN CONTENT-->
			<div id="content">
				<section>
					<div class="section-body">
						<div class="row">
							<div class="col-md-12"><h2>ESTADÍSTICAS	</h2></div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-content">
										<div class="card-body pl-0">
											<div class="height-300">
												<canvas id="simple-pie-chart"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card">
									<div class="card-content">
										<div class="card-body pl-0">
											<div class="height-300">
												<canvas id="bar-chart"></canvas>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-body no-padding">
										<div class="alert alert-callout alert-info no-margin">
											<strong class="pull-right text-success text-lg">{{ $total_cupones }}</strong>
											<strong class="text-xl">Total promociones ingresadas:</strong><br>
											<span class="opacity-50"></span>
											<div class="stick-bottom-left-right">
												<div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"><canvas style="display: inline-block; width: 259px; height: 80px; vertical-align: top;" width="259" height="80"></canvas></div>
											</div>
										</div>
									</div><!--end .card-body -->
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12"><h4>TOTAL DE CUPONES POR CATEGORÍA</h4></div>
							@if(count($totalxCategoria))
							@foreach($totalxCategoria as $list)
							<div class="col-md-2 col-sm-6">
								<div class="card">
									<div class="card-body no-padding">
										<div class="alert alert-callout alert-info no-margin">
											<strong class="pull-right text-success text-lg">{{$list->total}}</strong>
											<strong class="text-xl">{{$list->titulo}}</strong><br>
											<span class="opacity-50"></span>
											<div class="stick-bottom-left-right">
												<div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"><canvas style="display: inline-block; width: 259px; height: 80px; vertical-align: top;" width="259" height="80"></canvas></div>
											</div>
										</div>
									</div><!--end .card-body -->
								</div><!--end .card -->
							</div>
							@endforeach
							@endif
						</div><!--end .row -->						
					</div><!--end .section-body -->
				</section>
			</div><!--end #content-->
			<!-- END CONTENT -->
		</div><!--end #base-->
		<!-- END BASE -->
		<!-- end body dashboard -->
		@include('layout/menu')
		@include('layout/scripts')

		<script>
			var generos = {!! json_encode($generos) !!};
			var edades = {!! json_encode($edades) !!};
		</script>

		<script src="/js/charts/chart.min.js"></script>
		<script src="/js/charts/chart-chartjs.js"></script>

	</body>
</html>