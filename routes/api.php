<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//|-------------------------------------------------------------------------------------------------------------|
//|                                           RUTAS POST                                                        |
//|-------------------------------------------------------------------------------------------------------------|
//------------------------------------------------------------------------------------------(VALIDACION DE LOGIN)
//http://52.14.22.140/validarLogin
Route::POST('/validarLogin', 'Auth\LoginController@validarLoginMovil');
//-----------------------------------------------------------------------------------------(REGISTRO DE USUARIOS)
//http://52.14.22.140/registro
Route::POST('/registro', 'ApiController@registro');
//------------------------------------------------------------------------------------------(REGISTRO DE CUPONES)
//http://52.14.22.140/registroCupon
Route::POST('/registroCupon', 'ApiController@RegistroCupon');
//----------------------------------------------------------------------------------------------(REDIMIR CUPONES)
//http://52.14.22.140/redimirCupon
Route::POST('/redimirCupon', 'ApiController@RedimirCupon');
//http://52.14.22.140/favoritos
Route::POST('/favoritos', 'ApiController@favoritos');
//http://52.14.22.140/recuperar
Route::POST('/recuperar', 'ApiController@recoveryPass');
//http://52.14.22.140/verCupon
Route::POST('/verCupon', 'ApiController@vistaCupon');
//http://52.14.22.140/getcupones
Route::POST('/getcuponesCerca', 'ApiController@getcuponesCerca');

//http://52.14.22.140/updatePush
Route::POST('/updatePush', 'ApiController@updatePush');

//|-------------------------------------------------------------------------------------------------------------|
//|                                           RUTAS GET                                                         |
//|-------------------------------------------------------------------------------------------------------------|
//------------------------------------------------------------------------------------(GET LISTADO DE CATEGORIAS)
//http://52.14.22.140/categorias
Route::GET('/categorias', 'ApiController@getcategorias');
//---------------------------------------------------------------------------------------(GET LISTADO DE CUPONES)
//http://52.14.22.140/ofertas/id
Route::GET('/ofertas/{id}', 'ApiController@getofertaXcategoria');
//-----------------------------------------------------------------------------------(GET LISTADO CUPONES POR CANTIDAD DE VISTAS)
//http://52.14.22.140/getCuponesXvista
Route::GET('/getCuponesXvista', 'ApiController@getCuponesXvista');
//-----------------------------------------------------------------------------------(GET LISTADO DE ULTIMOS CUPONES)
//http://52.14.22.140/getUltimosCupones
Route::GET('/getUltimosCupones', 'ApiController@getUltimosCupones');
//-----------------------------------------------------------------------------------(GET LISTADO DE CUPONES)
//http://52.14.22.140/getcupones
Route::GET('/getcupones/{id}', 'ApiController@getcupones');
//-----------------------------------------------------------------------------------(GET LISTADO DE CUPONES POR USUARIO)
//http://52.14.22.140/cuponesUser/id
Route::GET('/cuponesUser/{id}', 'ApiController@getcuponesRedimidosXuser');
//http://52.14.22.140/misFavoritos/id
Route::GET('/misFavoritos/{id}', 'ApiController@misCategorias');
//http://52.14.22.140/misFavoritos/id
Route::GET('/categoriaNoselect/{id}', 'ApiController@CategoriaNoselect');