<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//-----------------------------------------------------------------------------------------------------------( CRUD LOGIN )
Route::get('/', function () { return view('Login/login'); });
Route::post('/validarLogin', 'Auth\LoginController@validarLogin');
Route::get('/logout', 'Auth\LoginController@Logout');

Route::get('/dashboard', 'DashboardController@index');

//-----------------------------------------------------------------------------------------------------------( CRUD CATEGORIAS )
Route::get('/categoria', 'CategoriaController@index');													//Listar
Route::get('/categorias', 'CategoriaController@categorias');											//Listar y sus cupones
Route::post('/crear.categoria', 'CategoriaController@store');											//Crear
Route::get('/eliminar.categoria/{id}', 'CategoriaController@delete');									//Eliminar	
Route::get('/editar.categoria/{id}', 'CategoriaController@edit');										//Editar	
Route::post('/actualizar.categoria/{id}', 'CategoriaController@update');								//Actualizar
//-----------------------------------------------------------------------------------------------------------( CRUD COMERCIOS )
Route::get('/comercio', 'ComercioController@index');													//Listar
Route::post('/crear.comercio', 'ComercioController@store');												//Crear
Route::get('/eliminar.comercio/{id}', 'ComercioController@delete');										//Eliminar	
Route::get('/editar.comercio/{id}', 'ComercioController@edit');											//Editar	
Route::post('/actualizar.comercio/{id}', 'ComercioController@update');									//Actualizar
//-----------------------------------------------------------------------------------------------------------( CRUD CUPONES )
Route::get('/cupones', 'CuponesController@index');														//Listar
Route::post('/crear.cupon', 'CuponesController@store');													//Crear
Route::get('/eliminar.cupon/{id}', 'CuponesController@delete');											//Eliminar	
Route::get('/editar.cupon/{id}', 'CuponesController@edit');												//Editar
Route::get('/vista.cupon/{id}', 'CuponesController@show');												//Editar	
Route::post('/actualizar.cupon/{id}', 'CuponesController@update');										//Actualizar
Route::get('/cupon/{id}', 'CuponesController@cupon');													//vista de cupones
//-----------------------------------------------------------------------------------------------------------( RUTA PAGINA WEB )
Route::get('/index', 'IndexController@index');
Route::get('/cupones/{id}', 'IndexController@cupones');
Route::get('/cuponesComercio/{id}', 'IndexController@cuponesComercio');
Route::post('/canjear', 'IndexController@canjear');
Route::post('/enviarCupon', 'IndexController@enviarCupon');
Route::post('/enviarCuponCategoria', 'IndexController@enviarCuponCategoria');
//-----------------------------------------------------------------------------------------------------------( Usuarios que ha redimido cupones )
Route::get('/cupones_redimidos', 'DashboardController@cupones');									    //Listar y sus cupones
Route::get('/usuarios', 'DashboardController@usuarios');											    //Listar y sus cupones
Route::get('/ver.cupones/{id}', 'DashboardController@cupones_user');											    //Listar y sus cupones
//-----------------------------------------------------------------------------------------------------------( LEVANTAR APP )
Route::get('/sendPush', 'IndexController@push');

Route::get('mail/send', 'MailController@send');
