<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuponesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupones', function (Blueprint $table) {
            $table->increments('idcupon');
            $table->Integer('idcomercio')->unsigned();
            $table->Integer('idcategoria')->unsigned();
            $table->string('titulo');
            $table->string('descripcion');
            $table->string('imagen');
            $table->date('fecha_creacion');
            $table->date('fecha_vencimiento');
            $table->string('codigo');
            $table->Integer('limite');
            $table->Integer('estado')->default(0);
            $table->Integer('visto');
            $table->string('redimido');
            $table->timestamps();

            //llave de comercio
            $table->foreign('idcomercio')->references('idcomercio')->on('comercio')->onDelete('cascade');
            $table->foreign('idcategoria')->references('idcategoria')->on('categoria')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupones');
    }
}
