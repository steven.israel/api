<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuponesUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupones_usuario', function (Blueprint $table) {
            $table->increments('idcupones_user');
            $table->Integer('idusuario')->unsigned();
            $table->Integer('idcupon')->unsigned();
            $table->timestamps();

            //llave de usuario y cupon
            $table->foreign('idusuario')->references('idusuario')->on('usuario')->onDelete('cascade');
            $table->foreign('idcupon')->references('idcupon')->on('cupones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupones_usuario');
    }
}
