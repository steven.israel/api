<?php

use Illuminate\Database\Seeder;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuario')->insert([
    		'nombre' => 'admin',
    		'apellido' => str_random(10),
    		'email' => 'admin@gmail.com',
            'idpais' => 1,
    		'fecha_nac' => '2018-07-01',
    		'pushtoken' => str_random(10),
    		'foto' => str_random(10),
    		'token_fb' => str_random(10),
    		'password' => bcrypt('secret'),
    		'remember_token' => str_random(10),

    	]);
    }
}
