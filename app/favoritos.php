<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class favoritos extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'favoritos';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idfavorito';

    /**
     * @var array
     */
    protected $fillable = ['idusuario','idcategoria', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'idusuario', 'idusuario');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'idcategoria', 'idcategoria');
    }
}
