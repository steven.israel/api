<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'categoria';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idcategoria';

    /**
     * @var array
     */
    protected $fillable = ['titulo', 'logo', 'color', 'created_at', 'updated_at'];

}
