<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Comercio;
use App\Categoria;
use App\Cupones;
use App\Usuario;
use App\CuponUsuario;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // obtener el total de usuario segun genero
        $masculino  = Usuario::where('sexo',1)->count();
        $femenino   = Usuario::where('sexo',2)->count();
        $generos    = [$masculino,$femenino];

        // obtenemos a los usuarios segun rango de edades
        $users      = Usuario::get(['edad']);
        $arr_edades  = [0,0,0,0,0];


        foreach ($users as $user) {
            if ($user->edad > 0 && $user->edad < 15) {
                $arr_edades[0] += 1;
            }else if ($user->edad > 15 && $user->edad < 30) {
                $arr_edades[1] += 1;
            }else if ($user->edad > 30 && $user->edad < 45) {
                $arr_edades[2] += 1;
            }else if ($user->edad > 45 && $user->edad < 60) {
                $arr_edades[3] += 1;
            }else if ($user->edad > 60) {
                $arr_edades[4] += 1;
            }
        }

        // contamos la cantidad de cupones ingresados
        $total_cupones  = Cupones::count();
        // obtenemos los cupones redimidos
        $datos          = CuponUsuario::all();
        

        $totalxCategoria = DB::select('SELECT count(c.idcategoria) as total, c.titulo FROM categoria as c INNER JOIN cupones as cu ON cu.idcategoria = c.idcategoria group by c.idcategoria, c.titulo');

        return view('Dashboard/dashboard')
                    ->with('generos', $generos)
                    ->with('edades',$arr_edades)
                    ->with('totalxCategoria', $totalxCategoria)
                    ->with('total_cupones',$total_cupones)
                    ->with('datos',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cupones()
    {
        $datos          = CuponUsuario::orderBy('created_at','DESC')->paginate(20);

        return view('Cupones/redimidos')->with('datos',$datos);
    }

    public function usuarios()
    {
        $datos = Usuario::orderBy('created_at','DESC')->paginate(10);

        return view('usuario/users')->with('datos',$datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cupones_user($id)
    {
        $user = Usuario::find($id);

        $datos = CuponUsuario::where('idusuario',$id)->orderBy('created_at','DESC')->paginate(20);

        return view('usuario/cupones')->with('datos',$datos)->with('user',$user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
