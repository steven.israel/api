<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Usuario;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

     /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function validarLogin(Request $request)
    {

        $this->validate($request, [
            'email'         => 'required|string|max:255',
            'password'      => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...

            return redirect()->intended('/dashboard');
        }

        return redirect()->intended('/')->withErrors('Usuario o Contraseña incorrectas');
    }

     /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function validarLoginMovil(Request $request)
    {

        $this->validate($request, [
            'email'         => 'required|string|max:255',
            'password'      => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...

            $usuario = Usuario::find(auth()->user()->idusuario);

            return '{
                        "errorCode": 1,
                        "user": '.$usuario.' 
                    }';
        }else{

            return '{
                        "errorCode": 0,
                        "errorMessage":"Correo o Contraseña incorrectos" 
                    }';
        }
    }

    public function Logout(Request $request)
    {

        $request->session()->regenerate(true);

        Auth::logout();


        return redirect('/');
    }

}
