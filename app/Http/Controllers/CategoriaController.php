<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use File;
use Image;
use App\Categoria;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $editar    = false;
        $datos     = Categoria::all();

        return view('/Categoria/categoria')->with('datos', $datos)->with('editar', $editar);
    }

    public function categorias()
    {
        // obtenemos las categorias
        $categorias = Categoria::all();

        return view('/Categoria/categorias')->with('categorias', $categorias);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/categoria');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 
            'titulo' => 'required|string|max:255',
            'icono'  => 'required',
            'color'  => 'required|string',
        ]);

        $ruta                   = public_path('/images/images_categoria/');

        $imagenOriginal         = $request->file('icono');

            // crear instancia de imagen
        $imagen  = Image::make($imagenOriginal);

            // generar un nombre aleatorio para la imagen
        $temp_name  = substr(strtoupper(sha1(time())), 0, 6) . '.' . $imagenOriginal->getClientOriginalExtension();

        $imagen->resize(300,300);

            // guardar imagen
            // save( [ruta], [calidad])
        $imagen->save($ruta . $temp_name, 100);

        $categoria              = new Categoria();
        $categoria->titulo      = $request->titulo;
        $categoria->logo        = $temp_name;
        $categoria->color        = $request->color;
        $categoria->save();

        $request->session()->flash('alert-info', 'Categoria Registrada');
        return redirect('/categoria');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editar     = true;
        $categoria  = Categoria::find($id);

        return view('/Categoria/categoria')->with('categoria', $categoria)->with('editar', $editar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator   = Validator::make($request->all(), [
            'titulo' => 'required|string|max:255',
            'color'  => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('/editar.categoria/'. $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $ruta                   = public_path('/images/images_categoria/');

        $imagenOriginal         = $request->file('icono');

        $categoria              = Categoria::find($id);
        $categoria->titulo      = $request->titulo;

        if (!empty($imagenOriginal)) {
            // crear instancia de imagen
            $imagen  = Image::make($imagenOriginal);

                // generar un nombre aleatorio para la imagen
            $temp_name  = substr(strtoupper(sha1(time())), 0, 6) . '.' . $imagenOriginal->getClientOriginalExtension();

            $imagen->resize(300,300);

                // guardar imagen
                // save( [ruta], [calidad])
            $imagen->save($ruta . $temp_name, 100);

            $categoria->logo        = $temp_name;

        }else{

            $categoria->logo        = $categoria->logo;
            
        }

            $categoria->color       = $request->color;
            $categoria->save();

        $request->session()->flash('alert-info', 'Categoria Actualizada');
        return redirect('/categoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        Categoria::destroy($id);

        $request->session()->flash('alert-info', 'Categoria Eliminada');
        return redirect('/categoria');
    }
}
