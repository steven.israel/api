<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Usuario;
use App\Categoria;
use App\Comercio;
use App\Cupones;
use App\CuponUsuario;
use App\favoritos;
use DB;
use App\Mail\sendMail;
use Illuminate\Support\Facades\Mail;

class ApiController extends Controller
{
    /**
     * registro de usuarios
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function Registro(Request $request)
    {
        try {
            
            $validator   = Validator::make($request->all(), [
                'email' => 'required|email|unique:usuario,email',
            ]);

            if ($validator->fails()) {

                $user = Usuario::where('email', $request->email)->get();
                $usuario = Usuario::find($user[0]->idusuario);

                return '{
                    "errorCode": 0,
                    "registro": "Ya existe el correo electronico" ,
                    "user": '.$usuario.'
                }';

            }else{

                $usuario                       = new Usuario();
                $usuario->email                = $request->email;
                $usuario->nombre               = $request->nombre;
                $usuario->apellido             = $request->apellido;
                $usuario->pushtoken            = $request->pushtoken;
                if (!empty($request->foto)) {
                    $usuario->foto             = $request->foto;
                }
                $usuario->token_fb             = $request->token_fb;
                $usuario->sexo                 = $request->sexo;
                $usuario->edad                 = $this->obtener_edad($request->fecha_nac);
                $usuario->fecha_nac            = $request->fecha_nac;
                $usuario->password             = bcrypt($request->password);
                $usuario->idpais               = $request->pais;
                $usuario->remember_token       = str_random(10);

                if ($usuario->save()) {

                    $usuario = Usuario::find($usuario->idusuario);

                    return '{
                        "errorCode": 1,
                        "user": '.$usuario.' 
                    }';
                }else{
                    return '{
                        "errorCode": 1,
                        "user": [] 
                    }';
                }
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": [] 
                    }';
        } 
    }

    public function obtener_edad($fecha){

        $fecha_nacimiento = $fecha;
        $dia_actual = date("Y-m-d");
        $edad_diff = date_diff(date_create($fecha_nacimiento), date_create($dia_actual));

        return $edad_diff->format('%y');
    }

    /**
     * actualizar pushtoken
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePush(Request $request)
    {
        try {

            $user = Usuario::find($request->id);

            $user->pushtoken = $request->push;

            if ( $user->save() ) {
                return '{
                    "errorCode": 1,
                    "cupones": "pushtoken actualizado" 
                }';
            }else{
                return '{
                    "errorCode": 2,
                    "cupones": "Error: intente nuevamente" 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }
    }

    /**
     * get lista categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function getcategorias()
    {
        try {
            
            $categorias = Categoria::all();

            if ( Count($categorias) > 0 ) {
                return '{
                    "errorCode": 1,
                    "categorias": '.$categorias.' 
                }';
            }else{
                return '{
                    "errorCode": 2,
                    "categorias": [] 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }
    }

    /**
     * get listado de ofertas por categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function getofertaXcategoria($id)
    {
        try {
            //$cupones = Cupones::all()->where('idcategoria', $id);

            $cupones = DB::select('SELECT 
                                    c.idcupon, c.titulo, c.descripcion, c.imagen, c.fecha_creacion, c.fecha_vencimiento, c.codigo, 
                                    c.limite, c.visto, c.redimido,ca.titulo as tituloCategoria, ca.logo as logoCategoria, 
                                    ca.color as colorCategoria, co.titulo as tituloComercio,co.codigo as codigoComercio, 
                                    co.descripcion as descripcionComercio, co.latitud, co.longitud, co.logo as logoComercio
                                    FROM cupones c
                                    INNER JOIN categoria ca ON ca.idcategoria = c.idcategoria 
                                    INNER JOIN comercio co ON co.idcomercio = c.idcomercio where c.idcategoria = '.$id.'');

            if ( count($cupones) > 0 ) {
                return '{
                    "errorCode": 1,
                    "cupones": '.json_encode($cupones).' 
                }';
            }else{
                return '{
                    "errorCode": 2,
                    "cupones": [] 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }
    }

     /**
     * get listado de cupones
     *
     * @return \Illuminate\Http\Response
     */
    public function getcupones($id)
    {
        try {
            $cupones = DB::select('SELECT c.idcupon, c.titulo, c.descripcion, c.imagen, c.fecha_creacion, c.fecha_vencimiento, c.codigo, 
                                    c.limite, c.visto, c.redimido,ca.idcategoria, ca.titulo as tituloCategoria, ca.logo as logoCategoria, 
                                    ca.color as colorCategoria, co.titulo as tituloComercio,co.codigo as codigoComercio, 
                                    co.descripcion as descripcionComercio, co.latitud, co.longitud, co.logo as logoComercio
                                    FROM cupones as c
                                    INNER JOIN categoria as ca ON ca.idcategoria = c.idcategoria 
                                    INNER JOIN comercio as co ON co.idcomercio = c.idcomercio
                                    INNER JOIN favoritos as f ON f.idcategoria = ca.idcategoria
                                    WHERE f.idusuario = '.$id.' AND c.estado = 1');

            if ( count($cupones)  > 0) {
            
                return '{
                    "errorCode": 1,
                    "cupones": '.json_encode($cupones).' 
                }';
                
            }else{
                return '{
                    "errorCode": 2,
                    "cupones": [] 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }       
    }

    /**
     * get listado de ofertas por categorias
     *
     * @return \Illuminate\Http\Response
     */
    public function getCuponesXvista()
    {
        try {
             $cupones = DB::select('SELECT 
                                    c.idcupon, c.titulo, c.descripcion, c.imagen, c.fecha_creacion, c.fecha_vencimiento, c.codigo, 
                                    c.limite, c.visto, c.redimido,ca.titulo as tituloCategoria, ca.logo as logoCategoria, 
                                    ca.color as colorCategoria, co.titulo as tituloComercio,co.codigo as codigoComercio, 
                                    co.descripcion as descripcionComercio, co.latitud, co.longitud, co.logo as logoComercio
                                    FROM cupones as c
                                    INNER JOIN categoria as ca ON ca.idcategoria = c.idcategoria 
                                    INNER JOIN comercio as co ON co.idcomercio = c.idcomercio 
                                    WHERE c.redimido <= limite AND c.estado = 1 and c.fecha_vencimiento >= NOW() ORDER BY c.visto DESC LIMIT 10');

            if ( count($cupones) > 0 ) {
                return '{
                    "errorCode": 1,
                    "cupones": '.json_encode($cupones).' 
                }';      
            }else{
                return '{
                    "errorCode": 2,
                    "cupones": [] 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }       
    }

    /**
     * registro de usuarios
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getcuponesCerca(Request $request)
    {
        try {
             $cupones = DB::select('SELECT DISTINCT c.idcupon,c.titulo,c.descripcion,c.imagen,c.fecha_creacion,c.fecha_vencimiento,c.codigo, c.limite,c.visto,c.redimido, ca.titulo as tituloCategoria, ca.logo as logoCategoria, ca.color as colorCategoria, co.titulo as tituloComercio, co.codigo as codigoComercio, co.descripcion as descripcionComercio, co.latitud, co.longitud, co.logo as logoComercio, TRUNCATE((6371* ACOS( COS( RADIANS('.$request->latitud.') ) * COS(RADIANS(co.latitud)) * COS(RADIANS(co.longitud) - RADIANS('.$request->longitud.') ) + SIN( RADIANS('.$request->latitud.') ) * SIN(RADIANS(co.latitud))) * 100), 0) AS distance FROM cupones as c, comercio as co, categoria as ca where c.idcomercio = co.idcomercio and c.idcategoria = ca.idcategoria and c.fecha_vencimiento >= 2018-12-23 and c.redimido <= c.limite AND c.estado = 1 group by c.idcupon HAVING (distance < 20);');

            //return date('Y-m-d');
            if ( count($cupones) > 0 ) {
                return '{
                    "errorCode": 1,
                    "cupones": '.json_encode($cupones).' 
                }';      
            }else{
                return '{
                    "errorCode": 2,
                    "cupones": [] 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }

       
    }


    /**
     * get listado ultimas de ofertas
     *
     * @return \Illuminate\Http\Response
     */
    public function getUltimosCupones()
    {
        try {
            $cupones = DB::select('SELECT 
                                    c.idcupon, c.titulo, c.descripcion, c.imagen, c.fecha_creacion, c.fecha_vencimiento, c.codigo, 
                                    c.limite, c.visto, c.redimido,ca.titulo as tituloCategoria, ca.logo as logoCategoria, 
                                    ca.color as colorCategoria, co.titulo as tituloComercio,co.codigo as codigoComercio, 
                                    co.descripcion as descripcionComercio, co.latitud, co.longitud, co.logo as logoComercio
                                    FROM cupones as c
                                    INNER JOIN categoria as ca ON ca.idcategoria = c.idcategoria 
                                    INNER JOIN comercio  as co ON co.idcomercio = c.idcomercio 
                                    WHERE c.redimido <= limite AND c.estado = 1 ORDER BY c.fecha_creacion DESC');

            if ( count($cupones) > 0 ) {
                return '{
                    "errorCode": 1,
                    "cupones": '.json_encode($cupones).' 
                }';
            }else{
                return '{
                    "errorCode": 2,
                    "cupones": [] 
                }';
            }
        } catch (Exception $e) {
            return '{
                        "errorCode": 5,
                        "error": '. $e .' 
                    }';
        }

        
    }

    /**
     * acctualizar vista de cupones
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function RegistroCupon(Request $request)
    {

        $cupon                       = new Cupones();
        $cupon->idcomercio           = $request->idcomercio;
        $cupon->idcategoria          = $request->idcategoria;
        $cupon->titulo               = $request->titulo;
        $cupon->descripcion          = $request->descripcion;
        $cupon->imagen               = $request->img;
        $cupon->fecha_creacion       = $request->fecha_crea;
        $cupon->fecha_vencimiento    = $request->fecha_ven;
        $cupon->codigo               = $request->codigo;
        $cupon->visto                = 0;
        $cupon->redimido             = 0;
        $cupon->limite               = $request->limite;

        if ($cupon->save()) {

            return '{
                "errorCode": 1,
                "registro": "Cupon Registrado" 
            }';
        }else{
            return '{
                "errorCode": 0,
                "registro": "Cupon no registrado" 
            }';
        }   
    }

    /**
     * redimir cupon
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function RedimirCupon(Request $request)
    {
        //verifico si el codigo del comercio es valido
        $VerificarCodigo = Comercio::all()->where('codigo', $request->codigoComercio)->count();

        if ( $VerificarCodigo > 0 ) {
            
            //si el codigo del comercio es valido procesido a verificar si el usuario ya ha redimido este cupon
            $redimido = CuponUsuario::all()->where('idcupon', $request->idcupon)->where('idusuario', $request->idusuario)->count();

            if ( $redimido > 0 ) {

                return '{
                    "errorCode": 0,
                    "msg": "El Cupon ya fue redimido" 
                }';

            }else{

                //si no ha sido redimido verifico que el cupon existe
                $cuponExist = Cupones::where('limite', '>', 'redimido')->where('idcupon', $request->idcupon)->get();
                //$cuponExist = DB::table('cupones')->where('redimido', '<', 'limite')->where('idcupon', $request->idcupon)->get();

                //return $cuponExist;
                if ( count($cuponExist) > 0) {
                    
                    $cupon                  = Cupones::find($request->idcupon);
                    $cupon->redimido        +=1 ;

                    if ($cupon->save()) {

                        $redimir = $this->canjearCupon($cupon->idcupon, $request->idusuario);

                        if ($redimir) {
                            
                            return '{
                                "errorCode": 1,
                                "msg": "Cupon Redimido" 
                            }';

                        }else{

                            return '{
                                "errorCode": 5,
                                "msg": "Cupon no redmido" 
                            }';

                        }
                    }else{

                        return '{
                            "errorCode": 2,
                            "msg": "No se actualizo la tabla cupones" 
                        }';

                    }   
                }else{
                    return '{
                            "errorCode": 4,
                            "msg": "Cupon Agotado" 
                        }';
                }

            }       
        }else{
            return '{
                        "errorCode": 3,
                        "msg": "Codigo de comercio invalido" 
                    }';
        }

    }

    public function canjearCupon($idcupon,$idusuario){

        $redimir             = new CuponUsuario();
        $redimir->idcupon    = $idcupon;
        $redimir->idusuario  = $idusuario;

        if ($redimir->save()) {
            return true;
        }

        return false;
    }

     /**
     * get listado de cupones redimidos por usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function getcuponesRedimidosXuser($id)
    {
        $cupones = DB::select('SELECT cu.idusuario, cu.idcupon, c.titulo, c.descripcion, c.imagen, c.fecha_creacion, c.fecha_vencimiento, c.codigo, 
                                    c.limite, c.visto, c.redimido, ca.titulo as tituloCategoria, ca.logo as logoCategoria, 
                                    ca.color as colorCategoria, co.titulo as tituloComercio,co.codigo as codigoComercio, 
                                    co.descripcion as descripcionComercio, co.latitud, co.longitud, co.logo as logoComercio
                                    FROM cupones_usuario as cu 
                                    INNER JOIN cupones as c ON c.idcupon = cu.idcupon
                                    INNER JOIN categoria AS ca ON ca.idcategoria = c.idcategoria 
                                    INNER JOIN comercio as co ON co.idcomercio = c.idcomercio where idusuario = '.$id.'');

        if ( count($cupones) > 0 ) {
            return '{
                "errorCode": 1,
                "cupones": '.json_encode($cupones).' 
            }';
        }else{
            return '{
                "errorCode": 2,
                "cupones": "[]" 
            }';
        } 
    }

    /**
     * categorias favoritas usuario
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function favoritos(Request $request)
    {

        $bandera = false;

        $categoriaDelete = favoritos::where('idusuario', $request->idusuario);

        $categoriaDelete->delete();

        foreach ($request->categorias as $value) {
            $favorito = new favoritos();

            $favorito->idusuario    = $request->idusuario;
            $favorito->idcategoria  = $value['idcategoria'];

            if($favorito->save()){
                $bandera = true;
            }    
        }

        if ($bandera) {
            return '{
                "errorCode": 1,
                "msg": "Categorias guardadas exitosamente"
            }';
        }
    }

    /**
     * categorias favoritas usuario
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function recoveryPass(Request $request)
    {
        //generar pass
        $pass = substr(strtoupper(sha1(time())), 0, 6);

        $usuario = Usuario::all()->where('email', $request->email);

        if ( count($usuario) > 0 ) {

            $objDemo = new \stdClass();
            $objDemo->email     = $usuario[0]->email;
            $objDemo->usuario   = $usuario[0]->nombre;
            $objDemo->pass      = "secret";
     
            Mail::to($request->email)->send(new sendMail($objDemo));

            $actualizar = Usuario::find($usuario[0]->idusuario);

            $actualizar->password = bcrypt($pass);

            if ($actualizar->save()) {
                return '{
                    "errorCode": 1,
                    "msg": "Contraseña enviada" 
                }';
            }else{
                return '{
                    "errorCode": 2,
                    "msg": "No se han guardado" 
                }';
            }

        }
    }

    /**
     * get listado de categorias por usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function misCategorias($id)
    {
        $categorias = DB::select('SELECT ca.idcategoria,ca.titulo,ca.logo,ca.color,ca.created_at,ca.updated_at,IF((fa.idusuario = '.$id.'),1,0) as micategoria FROM categoria as ca 
                                    LEFT OUTER JOIN favoritos as fa ON ca.idcategoria = fa.idcategoria and fa.idusuario = '.$id.'
                                    WHERE fa.idcategoria IS NULL 
                                    UNION
                                    SELECT ca.idcategoria,ca.titulo,ca.logo,ca.color,ca.created_at,ca.updated_at,IF((fa.idusuario = '.$id.'),1,0) as micategoria FROM categoria as ca
                                    RIGHT OUTER JOIN favoritos as fa ON ca.idcategoria = fa.idcategoria 
                                    WHERE (ca.idcategoria IS NOT NULL and fa.idusuario ='.$id.')');

        if ( count($categorias) > 0 ) {
            return '{
                "errorCode": 1,
                "categorias": '.json_encode($categorias).' 
            }';
        }else{
            return '{
                "errorCode": 2,
                "categorias": "[]" 
            }';
        } 
    }

    /**
     * suma visto cupon
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function vistaCupon(Request $request)
    {

        $vista = Cupones::find($request->idcupon);

        $vista->visto += 1; 

        if ($vista->save()) {
           return '{
                "errorCode": 1,
                "msg": true
            }';
        }else{
            return '{
                "errorCode": 1,
                "msg": false 
            }';
        }
    }

    /**
     * get listado de categorias no seleccionadas por usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function CategoriaNoselect($id)
    {
        $categorias = DB::select('SELECT * FROM categoria WHERE (idcategoria NOT IN (SELECT idcategoria FROM favoritos where idusuario = '.$id.'))');

        if ( count($categorias) > 0 ) {
            return '{
                "errorCode": 1,
                "categorias": '.json_encode($categorias).' 
            }';
        }else{
            return '{
                "errorCode": 2,
                "categorias": "[]" 
            }';
        } 
    }

}
