<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use FCM;
use App\Cupones;
use App\Comercio;
use App\Categoria;
use App\Mail\sendCupon;
use Illuminate\Support\Facades\Mail;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categorias     = Categoria::all();
        $cupones 		= Cupones::whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                    ->where('limite','>', 'redimido')
                                    ->orderBy('created_at', 'DESC')
                                    ->paginate(10);

        $comercios        = Comercio::all()->take(10);

        $cuponesVistos = Cupones::whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                    ->where('limite','>', 'redimido')
                                    ->orderBy('visto', 'DESC')
                                    ->paginate(10);

        return view('/pagina/index')->with('categorias', $categorias)->with('cupones', $cupones)->with('vistos', $cuponesVistos)->with('comercios', $comercios);
    }

    public function push(){

        $pushtokens = DB::select('SELECT pushtoken FROM usuario' );

        foreach ($pushtokens as $list) {
           $this->procesoSend($list->pushtoken);
        }

    }

    public function procesoSend($push){

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['tipo' => 1, 'mensaje' => '', 'push' => '']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = "eF2N8FNhGNk:APA91bF9CEuAWsX8kKptjUSI6gfXylqXb5GgoKY-OQNti2Z61JSQvFrkHp46MgK5d4-XY6WW2f6X5ggufS2p6ekHwHW32Pb-xe7rYn7FaLZVbetV7BHv6u-awD7zLqhcisUFpoIFTVI0";
        //echo "entro";
        $downstreamResponse = FCM::sendTo($push, $option,null, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

        //return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

        //return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cupones($id)
    {

    	$categoria 	= Categoria::find($id);
        $cupones 	= Cupones::where('idcategoria', $id)
                                ->whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                ->where('limite','>', 'redimido')
                                ->get();

        //return $cupones;
        return view('/pagina/cuponesCategoria')->with('cupones', $cupones)->with('categoria', $categoria);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cuponesComercio($id)
    {

        $categoria  = comercio::find($id);
        $cupones    = Cupones::where('idcomercio', $id)
                                ->whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                ->where('limite','>', 'redimido')
                                ->get();

        //return $cupones;
        return view('/pagina/cuponesComercio')->with('cupones', $cupones)->with('categoria', $categoria);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function canjear(Request $request)
    {

    	$comercios        = Comercio::all()->take(10);
    	if ($request->tipo == 1) {

    		$categorias     = Categoria::all();
	    	$cupones 		= Cupones::whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                        ->where('limite','>', 'redimido')
                                        ->orderBy('created_at', 'desc')->paginate(10);
            $cuponesVistos = Cupones::whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                    ->where('limite','>', 'redimido')
                                    ->orderBy('visto', 'DESC')
                                    ->paginate(10);
	    	$cupon 			= Cupones::find($request->cupon);

	        return view('/pagina/index')->with('cupones', $cupones)->with('categorias', $categorias)->with('canjear', 1)
                                        ->with('cupon', $cupon)
                                        ->with('vistos', $cuponesVistos)
                                        ->with('comercios', $comercios);
    	}else{

    		$categoria 	= Categoria::find($request->categoria);
        	$cupones   = Cupones::where('idcategoria', $request->categoria)
                                ->whereDate('fecha_vencimiento', '>=', DATE('Y-m-d'))
                                ->where('limite','>', 'redimido')
                                ->get();
        	$cupon 		= Cupones::find($request->cupon);

    		return view('/pagina/cuponesCategoria')->with('cupones', $cupones)->with('categoria', $categoria)->with('canjear', 1)->with('cupon', $cupon)->with('comercios', $comercios);
    	}
    }

    public function enviarCupon(Request $request){

        $cupon = Cupones::find($request->idcupon);

        $objDemo = new \stdClass();
        $objDemo->email         = $request->mail;
        $objDemo->titulo        = $cupon->titulo;
        $objDemo->descripcion   = $cupon->descripcion;
        $objDemo->imagen        = $cupon->imagen;
        $objDemo->codigo        = $cupon->codigo;

        Mail::to($request->mail)->send(new sendCupon($objDemo));

        return redirect('index');
    }

    public function enviarCuponCategoria(Request $request){

        $cupon = Cupones::find($request->idcupon);

        $objDemo = new \stdClass();
        $objDemo->email         = $request->mail;
        $objDemo->titulo        = $cupon->titulo;
        $objDemo->descripcion   = $cupon->descripcion;
        $objDemo->imagen        = $cupon->imagen;
        $objDemo->codigo        = $cupon->codigo;

        Mail::to($request->mail)->send(new sendCupon($objDemo));

        return redirect('/cupones/'.$cupon->idcategoria);
    }
}
