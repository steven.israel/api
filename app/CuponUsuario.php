<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuponUsuario extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cupones_usuario';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idcupones_user';

    /**
     * @var array
     */
    protected $fillable = ['idcupon', 'idusuario', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function cupones()
    {
        return $this->belongsTo('App\Cupones', 'idcupon', 'idcupon');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function usuarios()
    {
        return $this->belongsTo('App\Usuario', 'idusuario', 'idusuario');
    }
}
