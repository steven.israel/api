<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable ;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Usuario extends \Eloquent implements Authenticatable
{
    use AuthenticableTrait;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'usuario';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idusuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','apellido', 'email','edad','sexo','idpais','pushtoken','foto','token_fb','fecha_nac', 'password', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function pais()
    {
        return $this->belongsTo('App\Pais', 'idpais', 'idpais');
    }

    public function cupones_redimidos()
    {
        return $this->HasMany('App\CuponUsuario', 'idusuario', 'idusuario')->count();
    }
}
