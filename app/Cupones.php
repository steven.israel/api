<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cupones extends Model
{
     /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cupones';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idcupon';

    /**
     * @var array
     */
    protected $fillable = ['idcomercio', 'idcategoria','estado', 'titulo', 'descripcion', 'imagen', 'fecha_creacion', 'fecha_vencimiento', 'codigo', 'limite', 'visto', "redimido", 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function comercio()
    {
        return $this->belongsTo('App\Comercio', 'idcomercio', 'idcomercio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'idcategoria', 'idcategoria');
    }
}
